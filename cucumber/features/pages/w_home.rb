require 'site_prism'

module WALMART
  
  module Pages
  
    class HomePage < SitePrism::Page
      set_url '/'
      #section :header, WALMART::Sections::Header, '.headeralign'
      element :search, '#suggestion-search'
	  element :searchButton, '#site-topbar > div.wraper-right-icons > form > button'

      def searchText(text)
        search.set text
		searchButton.click
      end

    end

  end
  
end


