require 'site_prism'

module WALMART

  module Pages
    class ProductPage < SitePrism::Page
	
      element :productName, '#buybox > div > header > div.product-title-wrapper > h1'
	  element :cepField, '#estimate-shipping-txt-cep'
	  element :cepButton, '#buybox > div > div.buybox-tools.buybox-container > div.buybox-tools-item.buybox-tools-item-last.buybox-tools-postal-box > div > form > input.button-pill.button-default.small.light.inline.estimate-shipping-button.track-satellite-oneclick'
	  element :cepText, '#buybox > div > div.buybox-payments-container > div > div > div.product-sellers.buybox-container > ul > li > div.buybox-container-column.buybox-seller-shipping > a > span'
	  element :addToCartbutton, '#buybox > div > div.buybox-payments-container > div > div > div.buybox-actions.buybox-container.clearfix > button > span'
	  element :closeButton, 'class#mfp-close'
	  element :cartButton, '#site-topbar > div.wraper-right-icons > div.cart > a > span.cart-icon'
		  
	  def validateCorrectProduct(productListName, productName)
		if productListName[productName]
			puts "Adicionou produto ao carrinho"
		else
			puts "Não adicionou produto ao carrinho"
		end
	  end
	  
	  def insertCep(cep)
		cepField.set cep
		cepButton.click
	  end
	  
	  def validateCep
		if cepText.text["Calcular frete"]
			puts "Cep invalido"
		else
			puts "Cep valido"
		end
	  end

	 def addToCart
		addToCartbutton.click
		cartButton.click
	 end
	  
    end
  end
end
