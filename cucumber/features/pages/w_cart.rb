require 'site_prism'

module WALMART

  module Pages
    class CartPage < SitePrism::Page
	
      element :cartItems, '#main > div > section > div > div.page-title.clearfix > h2 > span'
      element :removeLink, '#main > div > section > div > article > ul > li > div > div.my-cart-product-item.my-cart-product-quantity > a'

      def validateAddToCart
	  
		if cartItems.text["1"]
			puts "Produto adicionado ao carrinho"
			removeLink.click
		else
			puts "Produto nao foi adicionado"
		end
      end
	  
    end
  end
end

