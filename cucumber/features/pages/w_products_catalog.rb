# encoding: utf-8

require 'site_prism'

module WALMART

    module Pages
        class ProductCatalogPage < SitePrism::Page

			@rand = Random.rand(1...4)
			element :product, "#product-list > section > ul > li.column.item-#{@rand}.shelf-product-item"		
			element :resultsCount, '#sort-by > span.result-items'
			element :productName, "#product-list > section > ul > li.column.item-#{@rand}.shelf-product-item > section > a > span"
			
			def chooseProduct
			name = productName.text
			product.click
			return name
			end
			
			def validateResultsCount
			
			#validate < 0
			
			end
			
        end
    end

end