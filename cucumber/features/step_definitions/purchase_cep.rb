﻿Dado(/^que eu estou na pagina principal do walmart$/) do
	homePage.load
end

Quando(/^coloco na pesquiso a palavra tv$/) do
	@pesquisa = CONFIG['pesquisa']['televisao']
	homePage.searchText(@pesquisa)
end

E(/^seleciono um produto$/) do
	puts productCatalogPage.resultsCount.text
	@productName = productCatalogPage.chooseProduct
end

E(/^insiro um cep invalido no frete$/) do
	productPage.validateCorrectProduct(@productName, productPage.productName.text)
	productPage.insertCep(CONFIG['cep']['invalido'])
end

Entao(/^vejo a mensagem de cep invalido$/) do
	productPage.validateCep
end