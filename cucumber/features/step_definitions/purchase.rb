﻿Dado(/^que eu estou na pagina principal$/) do
	homePage.load
end

Quando(/^pesquiso por tv$/) do
	@pesquisa = CONFIG['pesquisa']['televisao']
	homePage.searchText(@pesquisa)
end

E(/^seleciono um dos produto$/) do
	puts productCatalogPage.resultsCount.text
	@productName = productCatalogPage.chooseProduct
end

Entao(/^adiciono ao carrinho$/) do	
	productPage.validateCorrectProduct(@productName, productPage.productName.text)
	productPage.addToCart
	cartPage.validateAddToCart
end