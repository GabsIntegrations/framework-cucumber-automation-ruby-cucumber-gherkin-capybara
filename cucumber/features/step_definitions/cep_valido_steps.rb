﻿Dado(/^que eu tenho acesso a URL da chamada do serviço dos correios$/) do          
	@url = CONFIG['servicos_cep']['urlCorreiosCep']
end                                                                          

Quando(/^realizo uma requisição com um cep valido$/) do 
	@jsonEndereco = cepCorreiosService.doHttpGetRequest(@url, CONFIG['cep']['valido'])
end                                                                          

Entao(/^recebo uma resposta com dados de endereço$/) do                      
	puts @jsonEndereco
end                                                                          