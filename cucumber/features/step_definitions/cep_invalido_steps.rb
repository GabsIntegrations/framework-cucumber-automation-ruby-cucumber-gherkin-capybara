﻿Dado(/^que eu acesso a URL da chamada do serviço dos correios$/) do          
	@url = CONFIG['servicos_cep']['urlCorreiosCep']
end                                                                          

Quando(/^realizo uma requisição com um cep invalido$/) do 
	@jsonEndereco = cepCorreiosService.doHttpGetRequest(@url, CONFIG['cep']['invalido'])
end                                                                          

Entao(/^recebo uma resposta sem dados de endereço$/) do                      
	puts @jsonEndereco
end                                                                          