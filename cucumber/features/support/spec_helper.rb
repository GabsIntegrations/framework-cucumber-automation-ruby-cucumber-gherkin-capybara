
Dir[File.join(File.dirname(__FILE__), '../pages/*.rb')].each { |file| require file }

module WALMART
  
  module Pages
    
	def homePage
      WALMART::Pages::HomePage.new
    end

    def productPage
      WALMART::Pages::ProductPage.new
    end

    def productCatalogPage
      WALMART::Pages::ProductCatalogPage.new
    end

    def cartPage
      WALMART::Pages::CartPage.new
    end

  end
  
end
