
Dir[File.join(File.dirname(__FILE__), '../services/*.rb')].each { |file| require file }

module WALMART
  
  module Services
    
	def cepCorreiosService
      WALMART::Services::CepCorreiosServices.new
    end
  
  end
end
