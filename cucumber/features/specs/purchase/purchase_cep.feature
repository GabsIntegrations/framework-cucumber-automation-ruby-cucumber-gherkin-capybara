#language:pt

@compra
Funcionalidade: Compra

	@compra_NOK 
	Cenario: Compra de uma televisao
  
		Dado que eu estou na pagina principal
		Quando pesquiso por tv
		E seleciono um produto
		E insiro um cep invalido no frete
		Entao vejo a mensagem de cep invalido